<?php

namespace App\Http\Controllers\Backend;

use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function index() {
        return view('backend.settings', Settings::getSettings());
    }

    public function store(Request $request) {
        Settings::where('key', '!=', null)->delete();
        foreach ($request->except('_token') as $key => $value) {
            $settings = new Settings();
            $settings->key = $key;
            $settings->value = $request->$key;
            $settings->save();
        }
        return redirect()->route('admin.settings.index');
    }

    protected function sendTelegramData($route = '', $params = [], $method = 'POST') {
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://api.telegram.org/bot' . \Telegram::getAccessToken() . '/']);
        $result = $client->request($method, $route, $params);
        return (string) $result->getBody();
    }

    protected function setWebHook(Request $request) {
        $result = $this->sendTelegramData('setwebhook', [
            'query' => ['url' => $request->url . '/' . \Telegram::getAccessToken()]
        ]);
        return redirect()->route('admin.settings.index')->with('status', $result);
    }

    protected function getWebHookInfo(Request $request) {
        $result = $this->sendTelegramData('getWebhookInfo');
        return redirect()->route('admin.settings.index')->with('status', $result);
    }
}
