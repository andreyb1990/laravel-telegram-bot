<?php

namespace App\Http\Controllers\Backend;

use App\TelegramUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Telegram\Bot\Laravel\Facades\Telegram;

class TelegramController extends Controller
{
    public function webhook() {

        $telegram = Telegram::getWebhookUpdates()['message'];

        if (!TelegramUser::find($telegram['from']['id'])) {
            $telegram_user = new TelegramUser();
            $telegram_user->id = $telegram['from']['id'];
            $telegram_user->is_bot = $telegram['from']['is_bot'];
            $telegram_user->first_name = $telegram['from']['first_name'];
            $telegram_user->last_name = $telegram['from']['last_name'];
            $telegram_user->username = $telegram['from']['username'];
            $telegram_user->language_code = $telegram['from']['language_code'];
            $telegram_user->save();
        }

        Telegram::commandsHandler(true);
    }
}
