<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Backend\TelegramController;
use Telegram\Bot\Laravel\Facades\Telegram;

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->prefix('admin')->namespace('Backend')->name('admin.')->group(function () {
    Route::get('/', 'DashboardController@index')->name('index');
    Route::get('/settings', 'SettingsController@index')->name('settings.index');
    Route::post('/settings/store', 'SettingsController@store')->name('settings.store');
    Route::post('/settings/setwebhook', 'SettingsController@setWebHook')->name('settings.setwebhook');
    Route::post('/settings/getwebhookinfo', 'SettingsController@getWebHookInfo')->name('settings.getwebhookinfo');
});

Route::post(Telegram::getAccessToken(), function () {
    app('App\Http\Controllers\Backend\TelegramController')->webhook();
});

Auth::routes();

Route::match(['post', 'get'], 'register', function () {
    Auth::logout();
    return redirect('/');
})->name('register');

Route::get('/home', 'HomeController@index')->name('home');
