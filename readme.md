## Laravel Telegram Bot

This is starter kit for Telegram Bot creation using [Telegram Bot API - PHP SDK](https://telegram-bot-sdk.readme.io)

Installation
------------

##### Clone this repository repository

```console
$ git clone https://yourusername@bitbucket.org/andreyb1990/laravel-telegram-bot.git
```
##### Install composer packages
```console
$ composer install
```
##### Create .env file and set app name DB settings
```
APP_NAME=YourAppName
...
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=dbname
DB_USERNAME=dbusername
DB_PASSWORD=dbpassword
```
##### Generate Laravel key
```console
$ php artisan key:generate
```
##### Setup your username, email and password in UserTableSeeder
```php
[
    'name'              => 'Admin',
    'email'             => 'admin@example.com',
    'password'          => bcrypt('111'),
    'remember_token'    => str_random(10)
]
```
##### Run laravel migrations with seeders
```console
$ php artisan migrate --seed
```
##### Run [ngrok](https://ngrok.com/) to get public URL. If your are using MAMP server user this command:
```console
$ ./ngrok http -host-header=rewrite your-website.local:80
```
##### Set Telegram Bot webhook address
* Copy https public url from ngrok
* Go to **/login** page on your local website and login
* Got to **/admin/settings** page and insert the URL
* Press **Save** button to save URL in the database
* Press **Sendurl** button in dropdown menu to send webhook URL to the Telegram server

If everything done fine you will see response message like this:
```json
{"ok":true,"result":true,"description":"Webhook was set"}
```

Usage
------------
##### Go to Telegram facade (/config/telegram.php) and set Telegram Bot token
```php
'bot_token' => env('TELEGRAM_BOT_TOKEN', 'YOUR_BOT_TOKEN'),
```
##### Go to VerifyCsrfToken middleware (app/Http/Middleware/VerifyCsrfToken.php) and set first Telegram Bot token numbers (goes before ":" symbol)
```php
protected $except = [
    '000000000:*'
];
```
##### Create your own Telegram Bot commands
Create class for your command in App\Telegram namespace. You can user TestCommand class as a template.
Add your class to the commands list in Telegram facade
```php
'commands' => [
    Telegram\Bot\Commands\HelpCommand::class,
    App\Telegram\TestCommand::class,
]
```
##### Don't forget to update the composer autoloader after each change
```console
$ composer dump-autoload
```