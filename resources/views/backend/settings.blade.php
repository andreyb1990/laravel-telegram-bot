@extends('backend.layouts.app')

@section('content')
    <div class="container">
        @if (\Session::has('status'))
            <div class="alert alert-primary" role="alert">
                <span>{{ \Session::get('status')  }}</span>
            </div>
        @endif

        <form action="{{ route('admin.settings.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Callback URL</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary" type="submit">Save</button>
                        <button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item"
                               onclick="document.getElementById('url_callback_bot').value='{{ url('') }}'">Insert URL</a>
                            <a href="#" class="dropdown-item"
                               onclick="event.preventDefault(); document.getElementById('setwebhook').submit();"
                            >Send URL</a>
                            <a href="#" class="dropdown-item"
                               onclick="event.preventDefault(); document.getElementById('getwebhookinfo').submit();"
                            >Get info</a>
                        </div>
                    </div>
                    <input type="url" class="form-control" id="url_callback_bot" name="url_callback_bot" value="{{ $url_callback_bot }}">
                </div>
            </div>
        </form>

        <form id="setwebhook" action="{{ route('admin.settings.setwebhook') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
            <input type="hidden" name="url" value="{{ $url_callback_bot }}">
        </form>

        <form id="getwebhookinfo" action="{{ route('admin.settings.getwebhookinfo') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

    </div>
@endsection